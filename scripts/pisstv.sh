#!/bin/bash

cd HOMEDIR
while true; do
  echo taking
  date
  raspistill --width 320 --height 240 -o sstv.jpg
  echo superimposing
  date
  convert sstv.jpg -fill white -stroke black -pointsize 16 -annotate +50+50 'G3KMI de M0YCH\nQTH: University of Southampton\nWelcome!' sstv-out.jpg
  echo converting
  date
  ./pisstv/pisstv -r 48000 -p r36 sstv-out.jpg
  touch lock
  echo moving
  mv sstv-out.jpg.wav sstv-out.wav
  rm -f lock
  echo sending
  date
  while [ -f lock ]; do
    true
  done
  (while true; do cat sstv-out.wav; done) | csdr convert_i16_f \
  | csdr gain_ff 7000 | csdr convert_f_samplerf 20833 \
  | sudo ./rpitx -i- -m RF -f 145200
  echo repeating
  date
done
